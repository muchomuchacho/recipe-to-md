# `Down`load a recipe to Mark`down`

### Setup
* Install Poetry: `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python`
* Clone this repository: `git clone https://gitlab.com/muchomuchacho/recipe-to-md.git`
* `cd recipe-to-md`
* Install the `downrec` CLI tool: `poetry install`

### Details
##### The logic behind the way this works is somehow aligned to my personal workflow but it should be quite easy to adapt to others.
* It scrapes a recipe, converts it to markdown and saves the content to a `.md` file in your current working directory.
* Downloads the main image and places it in a folder of your choice relative to the location of the `.md` file.

Do all that with one simple command: `downrec 'Salmon With Courgette Ribbon & Feta Salad'` (Or any other recipe of your choice)